import csv
from selenium import webdriver
from selenium.common.exceptions import *
from xlsxwriter import Workbook

experts = {
'https://www.crunchbase.com/person/ranjeet-alexis'
,'https://www.crunchbase.com/person/uri-arazy'
,'https://www.crunchbase.com/person/vijay-reddy-2'
,'https://www.crunchbase.com/person/sean-doyle'
,'https://www.crunchbase.com/person/sean-doyle'
,'https://www.crunchbase.com/person/prasanna-sundararajan'
,'https://www.crunchbase.com/person/hillary-herlehy'
,'https://www.crunchbase.com/person/tim-bender'
,'https://www.crunchbase.com/person/scott-o-rourke'
,'https://www.crunchbase.com/person/gil-peleg'
,'https://www.crunchbase.com/person/alon-n-cohen'
,'https://www.crunchbase.com/person/benny-czarny'
,'https://www.crunchbase.com/person/roy-halevi'
,'https://www.crunchbase.com/person/gabi-heller'
,'https://www.crunchbase.com/person/dr-nitza-kardish'
,'https://www.crunchbase.com/person/eric-loh'
,'https://www.crunchbase.com/person/ellen-cohl'
,'https://www.crunchbase.com/person/rebeca-querub-2'
,'https://www.crunchbase.com/person/eduardo-shoval'
,'https://www.crunchbase.com/person/delia-ding'
,'https://www.crunchbase.com/person/al-gore'
,'https://www.crunchbase.com/person/brook-byers'
,'https://www.crunchbase.com/person/patrick-de-zeeuw'
,'https://www.crunchbase.com/person/javier-de-rocafort-8606'
,'https://www.crunchbase.com/person/ozden-pusat'
,'https://www.crunchbase.com/person/haldun-uraz-boralilar'
,'https://www.crunchbase.com/person/burak-erta'
,'https://www.crunchbase.com/person/demet-suzan-mutlu'
,'https://www.crunchbase.com/person/begm-tekin'
,'https://www.crunchbase.com/person/zara-apetrei-huseynova'
,'https://www.crunchbase.com/person/bahruz-mammadov'
,'https://www.crunchbase.com/person/rustam-mammadov'
,'https://www.crunchbase.com/person/vagit-alekperov'
,'https://www.crunchbase.com/person/rafael-azizov-4961'
,'https://www.crunchbase.com/person/turan-almammadov'
,'https://www.crunchbase.com/person/turan-sahinkaya'
,'https://www.crunchbase.com/person/turan-grler'
,'https://www.crunchbase.com/person/seyit-%C3%B6zg%C3%BCr'
,'https://www.crunchbase.com/person/rasim-andiran'
,'https://www.crunchbase.com/person/zuhtucan-soysal'
,'https://www.crunchbase.com/person/murat-yak%C4%B1c%C4%B1'
,'https://www.crunchbase.com/person/mauricio-palacio'
,'https://www.crunchbase.com/person/esteban-ochoa'
,'https://www.crunchbase.com/person/julia-hartz'
,'https://www.crunchbase.com/person/crystal-valentine'
,'https://www.crunchbase.com/person/galen-krumel'
,'https://www.crunchbase.com/person/marcos-iglesias'
,'https://www.crunchbase.com/person/mai-medhat'
,'https://www.crunchbase.com/person/nihal-fares'
,'https://www.crunchbase.com/person/dina-hadhoud'
,'https://www.crunchbase.com/person/melody-shin'
,'https://www.crunchbase.com/person/bryan-davis-2'
,'https://www.crunchbase.com/person/lisa-capps'
,'https://www.crunchbase.com/person/marcus-hiles-texas'
,'https://www.crunchbase.com/person/jakob-angele'
,'https://www.crunchbase.com/person/mohsin-ali-qureshi'
,'https://www.crunchbase.com/person/dmitry-falkovich'
,'https://www.crunchbase.com/person/mark-morabito'
,'https://www.crunchbase.com/person/greg-warner'
,'https://www.crunchbase.com/person/armando-gutierrez-jr'
,'https://www.crunchbase.com/person/vicco-von-bulow'
,'https://www.crunchbase.com/person/colin-hall'
,'https://www.crunchbase.com/person/marios-giorgoudis'
,'https://www.crunchbase.com/person/dimitris-kossyfas'
,'https://www.crunchbase.com/person/paris-thomas'
,'https://www.crunchbase.com/person/cyrus-tong'
,'https://www.crunchbase.com/person/kirk-stephens'
,'https://www.crunchbase.com/person/brandon-mulvihill'
,'https://www.crunchbase.com/person/bryan-reyhani'
,'https://www.crunchbase.com/person/lana-legostaeva'
,'https://www.crunchbase.com/person/blair-patacairk'
,'https://www.crunchbase.com/person/kara-eusebio'
,'https://www.crunchbase.com/person/lucy-lai'
,'https://www.crunchbase.com/person/jason-flick'
,'https://www.crunchbase.com/person/grant-courville'
,'https://www.crunchbase.com/person/irmantas-butkauskas'
,'https://www.crunchbase.com/person/tadas-jagminas'
,'https://www.crunchbase.com/person/mantas-katinas'
,'https://www.crunchbase.com/person/kadri-ugand'
,'https://www.crunchbase.com/person/erki-koldits'
,'https://www.crunchbase.com/person/peeter-saks'
,'https://www.crunchbase.com/person/martin-villig'
,'https://www.crunchbase.com/person/jaanus-tamm'
,'https://www.crunchbase.com/person/rain-rannu'
,'https://www.crunchbase.com/person/rando-rannus'
,'https://www.crunchbase.com/person/riina-einberg'
,'https://www.crunchbase.com/person/ragnar-sass'
,'https://www.crunchbase.com/person/liina-maria-lepik'
,'https://www.crunchbase.com/person/maarika-truu'
,'https://www.crunchbase.com/person/merilin-lukk'
,'https://www.crunchbase.com/person/mari-vavulski'
,'https://www.crunchbase.com/person/wole-soyinka'
,'https://www.crunchbase.com/person/oyebowale-akideinde'
,'https://www.crunchbase.com/person/fasan-mirazy'
,'https://www.crunchbase.com/person/opemipo-adeniyi'
,'https://www.crunchbase.com/person/olasimbo-sojinrin'
,'https://www.crunchbase.com/person/olasimbo-sojinrin'
,'https://www.crunchbase.com/person/adaora-udoji'
,'https://www.crunchbase.com/person/amina-oyagbola'
,'https://www.crunchbase.com/person/oyetayo-fred'
,'https://www.crunchbase.com/person/samuel-reed-2'
,'https://www.crunchbase.com/person/arthur-hayes'
,'https://www.crunchbase.com/person/kumar-dandapani'
,'https://www.crunchbase.com/person/vasja-zupan'
,'https://www.crunchbase.com/person/david-osojnik'
,'https://www.crunchbase.com/person/polona-b-bastar'
,'https://www.crunchbase.com/person/edward-kemp'
,'https://www.crunchbase.com/person/michel-leyers'
,'https://www.crunchbase.com/person/azer-guliev'
,'https://www.crunchbase.com/person/george-lambert'
,'https://www.crunchbase.com/person/george-kurtz'
,'https://www.crunchbase.com/person/george-wallace'
,'https://www.crunchbase.com/person/farhan-yaqub'
,'https://www.crunchbase.com/person/jacobo-guerra'
,'https://www.crunchbase.com/person/maryna-hradovich'
,'https://www.crunchbase.com/person/maryna-zakhareuskaya'
,'https://www.crunchbase.com/person/maryna-cherniavska'
,'https://www.crunchbase.com/person/maryna-burushkina'
,'https://www.crunchbase.com/person/maryna-vermishian'
}

expert_info_list = []
counter = 1
for expert in experts:
    print(str(counter)+expert)
    print("----------")
    counter = counter +1
    expert_info_dict = {}
    expert_info_label = ''
    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
    driver.get(expert)
    expert_profile_pic_url = 'http://127.0.0.1:8000/static/assets/images/no_photo.png'
    imgfield = driver.find_element_by_tag_name("image-with-fields-card")
    if len(imgfield.find_elements_by_tag_name('img')) > 0:
        expert_profile_pic_url = imgfield.find_element_by_tag_name('img').get_attribute('src')
    expert_info_dict["expert_profile_pic_url"] = expert_profile_pic_url
    expert_info = imgfield.find_elements_by_xpath(".//div[@class='ng-star-inserted']")
    expert_name = expert_info[0].text
    try:
        expert_btitle = expert_info[1].text
    except:
        expert_btitle = 'None'

    try:
        expert_company = expert_info[2].text
    except:
        expert_company = 'None'

    expert_info_dict['expert_name'] = expert_name
    expert_info_dict['expert_btitle'] = expert_btitle
    expert_info_dict['expert_company'] = expert_company

    fieldscard = driver.find_elements_by_tag_name("fields-card")
    if len(fieldscard) == 2:
        i = 1
        spanslabel = fieldscard[0].find_elements_by_xpath("//div[@class='layout-wrap layout-row']/span")
        for spanlabel in spanslabel:
            if i % 2 == 1:
                expert_info_label = spanlabel.text
            else:
                if expert_info_label in ('Facebook ', 'LinkedIn ', 'Twitter '):
                    spanvalue = spanlabel.find_element_by_tag_name('a').get_attribute("href")
                    expert_info_dict[expert_info_label] = spanvalue
                else:
                    expert_info_dict[expert_info_label] = spanlabel.text
            i = i + 1
    elif len(fieldscard) == 3:
        i = 1
        spanslabel = fieldscard[1].find_elements_by_xpath("//div[@class='layout-wrap layout-row']/span")
        for spanlabel in spanslabel:
            if i % 2 == 1:
                expert_info_label = spanlabel.text
            else:
                if expert_info_label in ('Facebook ', 'LinkedIn ', 'Twitter '):
                    spanvalue = spanlabel.find_element_by_tag_name('a').get_attribute("href")
                    expert_info_dict[expert_info_label] = spanvalue
                else:
                    expert_info_dict[expert_info_label] = spanlabel.text
            i = i + 1
    else:
        print("o hell no")

    if "Facebook " not in expert_info_dict:
        expert_info_dict["Facebook "] = 'None'

    if "Twitter " not in expert_info_dict:
        expert_info_dict["Twitter "] = 'None'

    if "LinkedIn " not in expert_info_dict:
        expert_info_dict["LinkedIn "] = 'None'

    try:
        desccard = driver.find_element_by_tag_name("description-card")
        if len(desccard.find_elements_by_xpath("a[@class='link-primary cb-display-inline ng-star-inserted']")) > 0:
            desccard.find_element_by_xpath("a[@class='link-primary cb-display-inline ng-star-inserted']").click()
        try:
            ps = desccard.find_elements_by_tag_name('p')
            for p in ps:
                expert_info_dict['text'] = p.text
        except:
            expert_info_dict['text'] = 'None'

        # print(desccard)
    except NoSuchElementException:
        expert_info_dict['text'] = 'None'
    # print(desccard)
    expert_info_list.append(expert_info_dict)
    driver.close()

# excel export

ordered_list = ["expert_profile_pic_url", "expert_name", "expert_btitle", "expert_company", "Location ", "Regions ",
                "Gender ", "text", "Investor Type ", "Investor Stage ","Number of Exits ", "Also Known As ", "Website ", "Facebook ", "LinkedIn ",
                "Twitter "]
wb = Workbook("crunch.xlsx")
ws = wb.add_worksheet("experts")

first_row = 0
for header in ordered_list:
    col = ordered_list.index(header)
    ws.write(first_row, col, header)

row = 1
for expert in expert_info_list:
    for _key, _value in expert.items():
        col = ordered_list.index(_key)
        ws.write(row, col, _value)
    row += 1
wb.close()
