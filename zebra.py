import csv
from selenium import webdriver
from selenium.common.exceptions import *
from xlsxwriter import Workbook

experts = {
    'https://www.zebra.com/tr/tr/products/mobile-computers/handheld.html'
}


counter = 1

for expert in experts:
    print(str(counter)+expert)
    print("----------")
    product_list = []
    counter = counter +1

    expert_info_label = ''
    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
    driver.get(expert)
    products = driver.find_elements_by_class_name("productseries")
    for product in products:
        prod_spec = {}
        bullets_list = ''
        bullets = product.find_elements_by_class_name("bullets")
        bullet_len = len(bullets)
        bullet_i = 1
        for bullet in bullets:
            if bullet_i == bullet_len:
                bullets_list = bullets_list + bullet.text
            else:
                bullets_list = bullets_list + bullet.text + '---'
            bullet_i = bullet_i + 1
        # print(bullet.text)
        desc_link = product.find_element_by_class_name("outbound-link").get_attribute('href')
        prod_spec['url'] = str(desc_link)
        prod_spec['bullets'] = str(bullets_list)
        product_list.append(prod_spec)
    print(product_list)
    driver.close()
