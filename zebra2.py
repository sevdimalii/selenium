import csv
from selenium import webdriver
from selenium.common.exceptions import *
from xlsxwriter import Workbook

experts = [{'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/ec30.html',
            'bullets': 'Kapalı ve Açık Mekanlarda Bas Konuş (PTT)---PBX Üzerinden Sesli Aramalar---Güvenli Mesajlaşma---Görev Yönetimi---Devam Durumu ve Süresinin Takibi---Fiyat Kontrolü ve Ürün Arama---Toplama/Paketleme---Temizlik/Bakım---Konum Takibi'},
           {'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/mc9300.html',
            'bullets': 'Envanter Yönetimi---Besleme Hattı İkmali---Güvenlik Testi---Kalite Kontrol Denetimleri---Parça Takibi---Alım/Saklama---İade İşlemleri---Fiyat Kontrolleri/Değişiklikleri---EDI (Elektronik Veri Alışverişi) İşlemleri/Toplama---Çapraz Yükleme Operasyonları'},
           {
               'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/tc72-tc77-series-touch-computer.html',
               'bullets': 'Fiyat Kontrolleri/Değişiklikleri---Envanter Yönetimi---Sesli İletişim---Sipariş/İade İşlemleri---Teslim Belgesi (imza/fotoğraf)---Paket/Palet Takip ve Tespit---Filo Yönetimi---Toplama/Saklama---Depo Yönetimi---Rota Optimizasyonu'},
           ]

product_spec_list1 = []
for cimis in experts:
    product_spec_list = {}
    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
    driver_pdf = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
    driver.get(cimis.get('url'))
    pp = driver.find_element_by_class_name('downloadcompatibility')
    pp_link = pp.find_element_by_tag_name('a').get_attribute('href')

    driver_pdf.get(pp_link)
    cc_action = driver_pdf.find_element_by_class_name('calltoaction')
    cc_action_link = cc_action.find_element_by_tag_name('a').get_attribute('href')
    product_spec_list["pdf_link"] = cc_action_link
    driver_pdf.close()
    bullet_list = cimis.get('bullets').split('---')
    kullanim_alani = '<ul>'
    for bullet in bullet_list:
        kullanim_alani = kullanim_alani + '<li>' + str(bullet) + '</li>'
    kullanim_alani = kullanim_alani + '</ul>'
    product_spec_list["kullanim_alani"] = kullanim_alani
    pr_images_section = driver.find_element_by_class_name("imageContainer")
    picture_cont = pr_images_section.find_element_by_tag_name("picture")
    pictures = picture_cont.find_elements_by_css_selector("*")
    pictures_list = ''
    pictures_list_len = len(pictures_list)
    pl_i = 1
    for picture in pictures:
        if pl_i == pictures_list_len:
            pictures_list = pictures_list + 'https://www.zebra.com' + picture.get_attribute('srcset')
        else:
            pictures_list = pictures_list + 'https://www.zebra.com' + picture.get_attribute('srcset') + ','
        pl_i = pl_i + 1

    product_spec_list["pictures_list"] = pictures_list
    product_overview = driver.find_element_by_class_name('product-overview')
    product_img_cont = product_overview.find_element_by_class_name('product--image')
    product_img_pic = product_img_cont.find_element_by_tag_name('picture')
    product_img = product_img_pic.find_element_by_tag_name('img').get_attribute('srcset')

    product_spec_list["product_img"] = 'https://www.zebra.com' + product_img
    name = product_overview.find_element_by_tag_name('h2').text
    description = product_overview.find_element_by_tag_name('p').text
    product_summary = driver.find_element_by_class_name('industry-use')
    product_spec_list["name"] = name
    product_spec_list["description"] = description

    ps_ul = product_summary.find_element_by_tag_name('ul')
    ps_lis = ps_ul.find_elements_by_tag_name('li')
    industries = '<ul style="list-style-type:none; margin-left:0px; margin-right:0px">'
    for industry in ps_lis:
        industries = industries + '<li list-style-type:none>' + industry.text + '</li>'
    industries = industries + '</ul>'
    product_spec_list["industries"] = industries
    prod_spec = driver.find_element_by_class_name('product--specification')
    spec_ul = prod_spec.find_element_by_class_name('specs')
    spec_lis = spec_ul.find_elements_by_tag_name('li')
    spec_list = '<ul>'
    for li in spec_lis:
        spec_title = li.find_element_by_tag_name('h3').text
        spec_value = li.find_element_by_tag_name('div').text
        spec_list = spec_list + '<li><p><span><strong>' + spec_title + '</strong></span></p>'
        spec_list = spec_list + '<div><p><span>' + spec_value + '</span></p></div>'
    spec_list = spec_list + '</ul>'
    product_spec_list["spec_list"] = spec_list
    product_spec_list1.append(product_spec_list)
    driver.close()

# excel export

ordered_list = ["pdf_link", "kullanim_alani", "pictures_list", "product_img", "name", "description", "industries",
                "spec_list"]
wb = Workbook("zebra.xlsx")
ws = wb.add_worksheet("zebra")

first_row = 0
for header in ordered_list:
    col = ordered_list.index(header)
    ws.write(first_row, col, header)

row = 1
print(product_spec_list1)
for expert in product_spec_list1:
    for _key, _value in expert.items():
        col = ordered_list.index(_key)
        ws.write(row, col, str(_value))
    row += 1
wb.close()
