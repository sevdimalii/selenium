import csv
from selenium import webdriver
from selenium.common.exceptions import *
from xlsxwriter import Workbook
import openpyxl
from pathlib import Path
import time

# getting companies

xlsx_file = Path(Path.home(), 'Desktop', 'selenium', 'crunch file.xlsx')
wb_obj = openpyxl.load_workbook(xlsx_file)
wsheet = wb_obj.active
companies = []
for row in wsheet.iter_rows():  # bura max_row argumenti ile maksimum line sayini muayyenlesdire bilersen,test ucun max_row=2 edirdim men
    for cell in row:
        companies.append(cell.value)

# scraping
company_info_list = []
ordered_list = ["company_logo_url", "company_name", "company_desc", "company_loc","text"]
for company in companies:
    print(company)
    company_info_dict = {}
    company_info_label = ''
    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
    driver.get(company)
    try:
        imgfield = driver.find_element_by_tag_name("image-with-fields-card")
        if len(imgfield.find_elements_by_tag_name('img')) > 0:
            company_logo_url = imgfield.find_element_by_tag_name('img').get_attribute('src')
        company_info_dict["company_logo_url"] = company_logo_url
        company_info = imgfield.find_elements_by_xpath(".//div[@class='ng-star-inserted']")

        company_name = company_info[0].text
        try:
            company_desc = company_info[1].text
        except:
            company_desc = 'blank'
        try:
            company_loc = company_info[2].text
        except:
            company_loc = 'blank'

        company_info_dict['company_name'] = company_name
        company_info_dict['company_desc'] = company_desc
        company_info_dict['company_loc'] = company_loc

        fieldscard = driver.find_elements_by_tag_name("fields-card")
        i = 1
        spanslabel = fieldscard[0].find_elements_by_xpath("//div[@class='layout-wrap layout-row']/span")
        for spanlabel in spanslabel:
            if i % 2 == 1:
                company_info_label = spanlabel.text
            else:
                if company_info_label in ('Facebook ', 'LinkedIn ', 'Twitter '):
                    spanvalue = spanlabel.find_element_by_tag_name('a').get_attribute("href")
                    company_info_dict[company_info_label] = spanvalue
                    if company_info_label not in ordered_list:
                        ordered_list.append(company_info_label)
                else:
                    if company_info_label not in ordered_list:
                        ordered_list.append(company_info_label)
                    company_info_dict[company_info_label] = spanlabel.text
            i = i + 1

            print(ordered_list)
        try:
            desccard = driver.find_element_by_tag_name("description-card")
            desccard.find_element_by_xpath("a[@class='link-primary cb-display-inline ng-star-inserted']").click()
            try:
                ps = desccard.find_elements_by_tag_name('p')
                for p in ps:
                    company_info_dict['text'] = p.text
            except:
                company_info_dict['text'] = 'blank'

            # print(desccard)
        except NoSuchElementException:
            print('desc not found')
        company_info_list.append(company_info_dict)
        driver.close()
    except:
        driver.close()
        continue

# ordered_list = ["company_logo_url", "company_name", "company_desc", "company_loc", "Industries ",
#                 "Headquarters Regions ", "Founded Date ", "Founders ", "Contact Email ", "Operating Status ",
#                 "Funding Status ", "Last Funding Type ", "Number of Employees ", "Legal Name ", "Also Known As ",
#                 "IPO Status ", "Company Type ", "Investor Type ", "Investment Stage ", "Website ", "LinkedIn ",
#                 "Twitter ", "Facebook ", "text", "Transaction Name ", "Acquired by ","Announced Date ","Phone Number ","Price ","Number of Enrollments ","Child Hubs "]

wb = Workbook("crunch-company.xlsx")
ws = wb.add_worksheet("companies")

first_row = 0
for header in ordered_list:
    col = ordered_list.index(header)
    ws.write(first_row, col, header)

row = 1
for company in company_info_list:
    for _key, _value in company.items():
        col = ordered_list.index(_key)
        ws.write(row, col, _value)
    row += 1
wb.close()
