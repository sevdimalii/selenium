import csv
from selenium import webdriver
from selenium.common.exceptions import *
from xlsxwriter import Workbook

products = [{'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/ec30.html',
             'bullets': 'Kapalı ve Açık Mekanlarda Bas Konuş (PTT)---PBX Üzerinden Sesli Aramalar---Güvenli Mesajlaşma---Görev Yönetimi---Devam Durumu ve Süresinin Takibi---Fiyat Kontrolü ve Ürün Arama---Toplama/Paketleme---Temizlik/Bakım---Konum Takibi---'},
            {'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/mc9300.html',
             'bullets': 'Envanter Yönetimi---Besleme Hattı İkmali---Güvenlik Testi---Kalite Kontrol Denetimleri---Parça Takibi---Alım/Saklama---İade İşlemleri---Fiyat Kontrolleri/Değişiklikleri---EDI (Elektronik Veri Alışverişi) İşlemleri/Toplama---Çapraz Yükleme Operasyonları---'},
            {
                'url': 'https://www.zebra.com/tr/tr/products/mobile-computers/handheld/tc72-tc77-series-touch-computer.html',
                'bullets': 'Fiyat Kontrolleri/Değişiklikleri---Envanter Yönetimi---Sesli İletişim---Sipariş/İade İşlemleri---Teslim Belgesi (imza/fotoğraf)---Paket/Palet Takip ve Tespit---Filo Yönetimi---Toplama/Saklama---Depo Yönetimi---Rota Optimizasyonu---'},
       ]

for product in products:
    # print(product.get('bullets'))
    bullet_list = product.get('bullets').split("---")
    for bullet in bullet_list:
        print(bullet)
