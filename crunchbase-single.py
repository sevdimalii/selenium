from selenium import webdriver
from selenium.common.exceptions import *
import urllib.request



expert_info_dict = {}
expert_info_label = ''
driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver/geckodriver')
driver.get("https://www.crunchbase.com/person/jiake-liu#section-overview")
imgfield = driver.find_element_by_tag_name("image-with-fields-card")
expert_profile_pic_url = imgfield.find_element_by_tag_name('img').get_attribute('src')
urllib.request.urlretrieve(expert_profile_pic_url,'cimi.png')
expert_info = imgfield.find_elements_by_xpath(".//div[@class='ng-star-inserted']")
expert_name = expert_info[0].text
try:
    expert_btitle = expert_info[1].text
except:
    expert_btitle = 'blank'

try:
    expert_company = expert_info[2].text
except:
    expert_company = 'blank'

expert_info_dict['expert_name'] = expert_name
expert_info_dict['expert_btitle'] = expert_btitle
expert_info_dict['expert_company'] = expert_company

fieldscard = driver.find_elements_by_tag_name("fields-card")
if len(fieldscard) == 2:
    i = 1
    spanslabel = fieldscard[0].find_elements_by_xpath("//div[@class='layout-wrap layout-row']/span")
    for spanlabel in spanslabel:
        if i % 2 == 1:
            expert_info_label = spanlabel.text
        else:
            if expert_info_label in ('Facebook ', 'LinkedIn ', 'Twitter '):
                spanvalue = spanlabel.find_element_by_tag_name('a').get_attribute("href")
                expert_info_dict[expert_info_label] = spanvalue
            else:
                expert_info_dict[expert_info_label] = spanlabel.text
        i = i + 1
elif len(fieldscard) == 3:
    i = 1
    spanslabel = fieldscard[1].find_elements_by_xpath("//div[@class='layout-wrap layout-row']/span")
    for spanlabel in spanslabel:
        if i % 2 == 1:
            expert_info_label = spanlabel.text
        else:
            if expert_info_label in ('Facebook ', 'LinkedIn ', 'Twitter '):
                spanvalue = spanlabel.find_element_by_tag_name('a').get_attribute("href")
                expert_info_dict[expert_info_label] = spanvalue
            else:
                expert_info_dict[expert_info_label] = spanlabel.text
        i = i + 1
else:
    None
try:
    desccard = driver.find_element_by_tag_name("description-card")
    if len(desccard.find_elements_by_xpath("a[@class='cb-link cb-display-inline ng-star-inserted']")) > 0:
        desccard.find_element_by_xpath("a[@class='cb-link cb-display-inline ng-star-inserted']").click()
    try:
        ps = desccard.find_elements_by_tag_name('p')
        expert_bio = ''
        for p in ps:
            expert_bio = expert_bio + p.text
        expert_info_dict['text'] = expert_bio
    except:
        expert_info_dict['text'] = 'blank'

    # print(desccard)
except NoSuchElementException:
    None
# print(desccard)
print(expert_info_dict)
driver.close()
